﻿using System;
using System.Collections.Generic;
using System.Text;
using Task17;

namespace Task17
{
    abstract class Class
    {
        protected string className;
        protected double abilityResource;
        protected double hpModifier = 1;
        protected double armorModifier = 1;

        protected Class()
        {
            className = "";
        }
        public string ClassName
        {
            get { return className; }
        }
        public double HpModifier
        {
            get { return hpModifier; }
        }
        public double ArmorModifier
        {
            get { return armorModifier; }
        }



        public abstract void Move();

        public abstract void Attack();
 
    }
}
