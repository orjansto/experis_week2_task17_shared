﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task17.Classes
{
    /// <summary>
    /// A RPG-class
    /// </summary>
    class Fighter : Class
    {
        public Fighter()
        {
            Stamina = 100;
            className += "Fighter";
            hpModifier *= 1.2;
            armorModifier *= 1.2;
        }
        public double Stamina
        {
            get { return abilityResource; }
            set { abilityResource = value; }
        }
       


        public override void Attack()
        {
            Console.WriteLine("Strikes with full force!");
        }

        public override void Move()
        {
            Console.WriteLine("Charges forward!");
        }
    }
}
