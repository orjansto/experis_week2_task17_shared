﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task17.Classes.Fighters
{
    /// <summary>
    /// A RPG Fighter Class
    /// </summary>
    class Darigan : Fighter
    {
        public Darigan()
        {
            className += " Darigan";
            hpModifier *= 0.9;
            armorModifier *= 1.1;
        }
    }
}
