﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task17.Classes.Fighters
{
    /// <summary>
    /// A RPG Fighter Class
    /// </summary>
    class Warrior : Fighter
    {
        public Warrior()
        {
            className += " Warrior";
        }
    }
}
