﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task17.Classes
{
    /// <summary>
    /// A RPG-class
    /// </summary>
    class Rogue : Class
    {
        public Rogue()
        {
            Energy = 100;
            className += "Rogue";
            hpModifier *= 1;
            armorModifier *= 1;
        }
        public double Energy
        {
            get { return abilityResource; }
            set { abilityResource = value; }
        }
        

        public override void Attack()
        {
            Console.WriteLine("You stab the enemy.");
        }

        public override void Move()
        {
            Console.WriteLine("..You sneak lightly forward..");
        }
    }
}
