﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task17.Classes.Rogues
{
    /// <summary>
    /// A RPG Rogue class
    /// </summary>
    class Smuggler : Rogue
    {
        public Smuggler()
        {
            Energy -= 10;
            className += " Smuggler";
            hpModifier *= 1.1;
            armorModifier *= 1.1;
        }
    }
}
