﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task17.Classes.Rogues
{
    /// <summary>
    /// A RPG Rogue class
    /// </summary>
    class Thief : Rogue
    {
        public Thief()
        {
            className += " Thief";
            hpModifier *= 0.9;
            armorModifier *= 0.9;
        }
    }
}
