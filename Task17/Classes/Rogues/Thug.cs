﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task17.Classes.Rogues
{
    /// <summary>
    /// A RPG Rogue class
    /// </summary>
    class Thug : Rogue
    {
        public Thug()
        {
            Energy += 5;
            className += " Thug";
            hpModifier *= 1.05;
            armorModifier *= 1.05;
        }
    }
}
