﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task17.Classes
{
    /// <summary>
    /// A RPG-class
    /// </summary>
    class Spellcaster : Class
    {
        
        public Spellcaster()
        {
            Mana = 100;
            className += "Spellcaster";
            hpModifier *= 0.8;
            armorModifier *= 0.8;
        }
        public double Mana
        {
            get { return abilityResource; }
            set { abilityResource = value; }
        }
        

        public override void Attack()
        {
            Console.WriteLine("You cast a spell at your enemy!");
        }

        public override void Move()
        {
            Console.WriteLine("You fumble ahead..");
        }
    }
}
