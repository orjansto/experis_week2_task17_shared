﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task17.Classes.Spellcasters
{
    /// <summary>
    /// A RPG Spellcaster class
    /// </summary>
    class Necromancer : Spellcaster
    {
        
        public Necromancer()
        {
            Mana *= 1.1;
            className += " Necromancer";
            hpModifier *= 0.9;
            armorModifier *= 0.9;
        }
    }
}
