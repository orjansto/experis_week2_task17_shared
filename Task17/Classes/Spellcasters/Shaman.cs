﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task17.Classes.Spellcasters
{
    /// <summary>
    /// A RPG Spellcaster class
    /// </summary>
    class Shaman : Spellcaster
    {
        public Shaman()
        {
            Mana *= 0.9;
            className += " Shaman";
            hpModifier *= 1.5;
            armorModifier *= 1.5;
        }
    }
}
